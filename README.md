# README #

This is a colour picker app that uses spectrum (jQuery extension) to do what is required for the job.

### What is this repository for? ###

* Ability to choose colour from the colour picker and sending rgb values to backend
* Saving rgb values to an xml file using PHP
* reading rgb values from an xml file using PHP